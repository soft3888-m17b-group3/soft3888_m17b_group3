import unittest
from util_test import TestUtilMethod
from database_test import TestDataBaseMethods
from event_test import TestEventMethods

"""run all testcases"""
suite = unittest.TestSuite()

tests_database = [TestDataBaseMethods("test_connect"),
                    TestDataBaseMethods("test_reader"),
                    TestDataBaseMethods("test_get_data"),
                    TestDataBaseMethods("test_get_wakeup_time"),
                    TestDataBaseMethods("test_get_temperature"),
                    TestDataBaseMethods("test_get_command"),
                    TestDataBaseMethods("test_change_time"),
                    TestDataBaseMethods("test_change_time_2"),
                    TestDataBaseMethods("test_change_time_3"),
                    TestDataBaseMethods("test_change_time_4"),
                    TestDataBaseMethods("test_change_time_5"),
                    TestDataBaseMethods("test_change_time_6"),
                    TestDataBaseMethods("test_change_time_7"),
                    TestDataBaseMethods("test_change_time_8")
                ]

tests_util = [TestUtilMethod("test_read_config"),
                TestUtilMethod("test_turn_off"),
                TestUtilMethod("test_api_call"),
                TestUtilMethod("test_get_rain"),
                TestUtilMethod("test_get_cloud"),
                TestUtilMethod("test_get_season")
            ]

tests_event = [TestEventMethods("test_fade_light_colour"),
                TestEventMethods("test_fade_light_brightness"),
                TestEventMethods("test_fade_light_out"),
                TestEventMethods("test_loop_music_sunny"),
                TestEventMethods("test_loop_music_rainy"),
                TestEventMethods("test_loop_music_normal"),
                TestEventMethods("test_loop_movie_normal"),
                TestEventMethods("test_loop_movie_rainy"),
                TestEventMethods("test_loop_movie_sunny"),
                TestEventMethods("test_do_action_fade"),
                TestEventMethods("test_do_action_movie"),
                TestEventMethods("test_do_action_music"),
                TestEventMethods("test_do_action_weather"),
                TestEventMethods("test_do_action_weather_rainy"),
                TestEventMethods("test_do_action_weather_sunny")
            ]

suite.addTests(tests_database)
suite.addTests(tests_util)
suite.addTests(tests_event)

runner = unittest.TextTestRunner(verbosity=1)

runner.run(suite)

