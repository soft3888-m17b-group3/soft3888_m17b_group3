import time
import database
from event import Event
import _thread
import sys
sys.path.append('/etc/openhab2/scripts/control')
import util

# IR things
def temperature_control():
    temperature,mode=database.get_temperature()
    command=database.get_command(temperature,mode)
    util.api_call("IRRemote",command)

_thread.start_new_thread ( temperature_control, () )

all_data=database.get_data()
events={}
for data in all_data:
    temp=Event(data[0],data[1],data[2])
    events[temp.get_start()]=temp
# print(events)

while True:
    time_now = time.strftime("%H:%M", time.localtime())
    if time_now in events.keys():
        events[time_now].do_action()
        time.sleep(61)