from bottle import route, run, template,redirect,request,static_file
import os
import sys
sys.path.append('/etc/openhab2/scripts/control')
import database
import util
import json

# @route('/exec')
# def exec():
#     try:
#         config=util.read_config()
#         path=config['SETTING']['root_path']
#         os.system('''ps -ef | grep {}/ | cut -c 9-15 | xargs kill -9'''.format(path)) 
#         os.system("setsid python3 "+path+"/{} &".format(config['SETTING']['include']))
#         os.system('\n echo "system_run.py up"')
#         # files=os.listdir(path)
#         # exclude=config['SETTING']['exclude'].split(",")
#         # for f in files:
#         #     if f in exclude:
#         #         continue
#         #     else:
#         #         os.system("setsid python3 "+path+"/"+f+" &")
#         #         os.system('\n echo "{} up"'.format(f))
#     except Exception:
#         pass

def takeOrder(elem):
    return elem[4]

@route('/js/<js:path>')
def serve_js(js):
    return static_file(js, root='static/js/')

@route('/fonts/<fonts:path>')
def serve_fonts(fonts):
    return static_file(fonts, root='static/fonts/')

@route('/css/<css:path>')
def serve_css(css):
    return static_file(css, root='static/css/')

@route('/')
def index():
    return redirect("/admin")

@route('/admin',method='GET')
def admin():
    name=[]
    time=[]
    action=[]
    ids=[]
    order=[]
    all_data=database.get_data()
    
    all_data.sort(key=takeOrder)

    for data in all_data:
        name.append(data[0])
        time.append(data[1])
        actions=json.loads(data[2])
        ids.append(data[3])
        order.append(data[4])
        temp_list=[]
        for key in actions:
            temp={}
            temp[key]=actions[key]
            temp_list.append(temp)
        action.append(temp_list)
    return template("admin",time=time, name=name, action=action, ids=ids, order=order)

    
@route('/admin',method='POST')
def do_admin():
    hour = request.forms.get('hour')
    minute = request.forms.get('minute')
    database.change_time(hour, minute)

    return redirect("/admin")

@route('/order',method='POST')
def do_order():
    datas=json.loads(request.forms.get("data"))
    orders={}
    for i in range(len(datas)):
        orders[i]=datas[i]["id"]
    database.change_order(orders)

# @route('/order',method='get')
# def order():
#     name=[]
#     time=[]
#     action=[]
#     ids=[]
#     order=[]
#     all_data=database.get_data()
    
#     all_data.sort(key=takeOrder)

#     for data in all_data:
#         name.append(data[0])
#         time.append(data[1])
#         actions=json.loads(data[2])
#         ids.append(data[3])
#         order.append(data[4])
#         temp_list=[]
#         for key in actions:
#             temp={}
#             temp[key]=actions[key]
#             temp_list.append(temp)
#         action.append(temp_list)
#     return template("order",time=time, name=name, action=action, ids=ids, order=order)

run(host='0.0.0.0', port=9999, reloader=True,debug=True)
