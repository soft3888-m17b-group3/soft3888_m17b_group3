from unittest.mock import Mock
from unittest import mock
import unittest
import time
from unittest.mock import patch
from util import api_call
import util
from event import Event

class TestEventMethods(unittest.TestCase):
    def setUp(self):
        
        return
    
    @patch("util.api_call")
    def test_fade_light_colour(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"lightBulbsColor":{"command":"60,80,60","fade_type":"color","fadein_min":"0","fadein_slot":"0","multhread":"fade_in","weather":"None"}}'
        event = Event(testName,testTimeSlot,testJson)
        key = "lightBulbsColor"
        event.fade_light(key)
        self.assertTrue(mocked_api.called)
        return
    
    @patch("util.api_call")
    def test_fade_light_brightness(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"lightBulbsColor":{"command":"60,80,60","fade_type":"brightness","fadein_min":"0","fadein_slot":"0","multhread":"fade_in","weather":"None"}}'
        event = Event(testName,testTimeSlot,testJson)
        key = "lightBulbsColor"
        event.fade_light(key)
        self.assertTrue(mocked_api.called)
    

    @patch("util.api_call")
    def test_fade_light_out(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"lightBulbsColor":{"command":"60,80,60","fade_type":"out","fadein_min":"0","fadein_slot":"0","multhread":"fade_in","weather":"None"}}'
        event = Event(testName,testTimeSlot,testJson)
        key = "lightBulbsColor"
        event.fade_light(key)
        self.assertTrue(mocked_api.called)

    @patch("util.api_call")
    @patch("event.time.strftime",mock.MagicMock(return_value="0:0"))
    @patch("event.util.get_rain",mock.MagicMock(return_value=0.0))
    @patch("event.util.get_cloud",mock.MagicMock(return_value=0.0))
    def test_loop_music_sunny(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"nestPlay":{"command":"None","weather":{"sunny":{"url":"test_url","length":"0:0"}}}}'
        event = Event(testName,testTimeSlot,testJson)
        key = "nestPlay"
        event.loop_music(key)
        self.assertTrue(mocked_api.called)

    @patch("util.api_call")
    @patch("event.time.strftime",mock.MagicMock(return_value="0:0"))
    @patch("event.util.get_rain",mock.MagicMock(return_value=50.0))
    @patch("event.util.get_cloud",mock.MagicMock(return_value=50.0))
    def test_loop_music_rainy(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"nestPlay":{"command":"None","weather":{"rainy":{"url":"test_url","length":"0:0"}}}}'
        event = Event(testName,testTimeSlot,testJson)
        key = "nestPlay"
        event.loop_music(key)
        self.assertTrue(mocked_api.called)
    
    @patch("util.api_call")
    def test_loop_music_normal(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"nestPlay":{"command":"test_url","weather":"None"},"length":"0:0"}'
        event = Event(testName,testTimeSlot,testJson)
        key = "nestPlay"
        event.loop_music(key)
        self.assertTrue(mocked_api.called)

    @patch("util.api_call")
    def test_loop_movie_normal(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"command":"test_url","weather":"None"},"length":"0:0"}'
        event = Event(testName,testTimeSlot,testJson)
        key = "chromecastPlay"
        event.loop_movie(key)
        self.assertTrue(mocked_api.called)

    @patch("util.api_call")
    @patch("event.time.strftime",mock.MagicMock(return_value="0:0"))
    @patch("event.util.get_rain",mock.MagicMock(return_value=50.0))
    @patch("event.util.get_cloud",mock.MagicMock(return_value=50.0))
    def test_loop_movie_rainy(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"command":"None","weather":{"rainy":{"url":"test_url","length":"0:0"}}}}'
        event = Event(testName,testTimeSlot,testJson)
        key = "chromecastPlay"
        event.loop_movie(key)
        self.assertTrue(mocked_api.called)

    @patch("util.api_call")
    @patch("event.time.strftime",mock.MagicMock(return_value="0:0"))
    @patch("event.util.get_rain",mock.MagicMock(return_value=0.0))
    @patch("event.util.get_cloud",mock.MagicMock(return_value=0.0))
    def test_loop_movie_sunny(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"command":"None","weather":{"sunny":{"url":"test_url","length":"0:0"}}}}'
        event = Event(testName,testTimeSlot,testJson)
        key = "chromecastPlay"
        event.loop_movie(key)
        self.assertTrue(mocked_api.called)

    @patch("event._thread.start_new_thread")
    def test_do_action_fade(self,mocked_thread):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"multhread":"fade"}}'
        event = Event(testName,testTimeSlot,testJson)
        event.do_action()
        self.assertTrue(mocked_thread.called)
    
    @patch("event._thread.start_new_thread")
    def test_do_action_movie(self,mocked_thread):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"multhread":"movie"}}'
        event = Event(testName,testTimeSlot,testJson)
        event.do_action()
        self.assertTrue(mocked_thread.called)
    
    @patch("event._thread.start_new_thread")
    def test_do_action_music(self,mocked_thread):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"multhread":"music"}}'
        event = Event(testName,testTimeSlot,testJson)
        event.do_action()
        self.assertTrue(mocked_thread.called)

    @patch("util.api_call")
    def test_do_action_weather(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"multhread":"None","weather":"None","command":"testCommand"}}'
        event = Event(testName,testTimeSlot,testJson)
        event.do_action()
        self.assertTrue(mocked_api.called)
    
    @patch("util.api_call")
    @patch("event.util.get_rain",mock.MagicMock(return_value=50.0))
    @patch("event.util.get_cloud",mock.MagicMock(return_value=50.0))
    def test_do_action_weather_rainy(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"multhread":"None","weather":{"rainy":"testCommand"},"command":"None"}}'
        event = Event(testName,testTimeSlot,testJson)
        event.do_action()
        self.assertTrue(mocked_api.called)


    @patch("util.api_call")
    @patch("event.util.get_rain",mock.MagicMock(return_value=0.0))
    @patch("event.util.get_cloud",mock.MagicMock(return_value=0.0))
    def test_do_action_weather_sunny(self,mocked_api):
        testName = "test"
        testTimeSlot = "9:30-10:30"
        testJson = '{"chromecastPlay":{"multhread":"None","weather":{"sunny":"testCommand"},"command":"None"}}'
        event = Event(testName,testTimeSlot,testJson)
        event.do_action()
        self.assertTrue(mocked_api.called)
if __name__ == "__main__":
    unittest.main()
    pass
    














