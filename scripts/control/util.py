import configparser
import requests
import time
from requests.auth import AuthBase
from requests.auth import HTTPBasicAuth
import datetime

def read_config():
    """Reads the configuration file"""
    config = configparser.ConfigParser()
    config.read('/etc/openhab2/scripts/control/config.ini')
    # config.read('config.ini')
    return config

def turn_off():
    """Turns off the system"""
    api_call("systemControl","OFF")
    api_call("ruleTest","ON")
    api_call("ruleTest","OFF")
    time.sleep(1) 

def api_call(item,command):
    """Sends a POST request to the openhab REST api for the given item and command"""
    auth = HTTPBasicAuth('soft3888', 'aK7is&jIn9O0s(')
    config=read_config()
    url=config['SETTING']['root_url']+"/rest/items/"
    headers = {
            'Content-Type': 'text/plain'
            }
    requests.request("POST", url+item, headers=headers, data=command, auth=auth)
    
def get_rain():
    """Gets the current rain in terms of mm/hr"""
    config = read_config()
    CITY_ID = config['SETTING']['CITY_ID']
    API_KEY = config['SETTING']['API_KEY']
    #CITY_ID = "2147714"
    #API_KEY = "4a3ea912a0624b7da97e09d6551e3d7e"
    URL = "http://api.weatherbit.io/v2.0/current?city_id={}&key={}".format(CITY_ID, API_KEY)
    response = requests.get(URL)
    if response.status_code == 200:
        # getting data in the json format
        data = response.json()['data'][0]
        rain = data['precip']
        return rain
    else:
        # showing the error message
        print("Error in the HTTP request to {} with status code {}".format(URL, response.status_code))

def get_cloud():
    """Gets the current cloud coverage as a percentage value"""
    config = read_config()
    CITY_ID = config['SETTING']['CITY_ID']
    API_KEY = config['SETTING']['API_KEY']
    #CITY_ID = "2147714"
    #API_KEY = "4a3ea912a0624b7da97e09d6551e3d7e"
    URL = "http://api.weatherbit.io/v2.0/current?city_id={}&key={}".format(CITY_ID, API_KEY)
    response = requests.get(URL)
    if response.status_code == 200:
        # getting data in the json format
        data = response.json()['data'][0]
        cloud = data['clouds']
        
        return cloud
    else:
        # showing the error message
        print("Error in the HTTP request to {} with status code {}".format(URL, response.status_code))

def get_season():
    """Returns the current season as a string"""
    x = datetime.datetime.now()
    month = int(x.strftime("%m"))
    val=""
    if month >= 3 and month <= 5:
        # Autumn
        val = "autumn"
    elif month >= 6 and month <= 8:
        # Winter
        val = "winter"
    elif month >= 9 and month <= 11:
        # Spring
        val = "spring"
    else:
        # Summer
        val = "summer"
    return val