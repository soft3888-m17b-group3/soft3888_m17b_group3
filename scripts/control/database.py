import sys
sys.path.append('/etc/openhab2/scripts/control')
import sqlite3
import util
import json
import datetime
import time

"""Database.py is responsible for read data from database """

"""create a database connection"""
def connect():
    config = util.read_config()
    conn = sqlite3.connect(config['SETTING']['database'])
    return conn


"""read database"""
def reader(event):
    connection=connect()
    cursor = connection.cursor()
    command='''SELECT time_slot from timing where event="{}";'''.format(event)
    cursor.execute(command)
    res=cursor.fetchone()[0]
    return res


"""get all data in database"""
def get_data():
    result = []
    connection=connect()
    cursor = connection.cursor()
    command='''SELECT time_slot,event,actions,id,eorder from timing ORDER BY eorder ASC;'''
    cursor.execute(command)
    res=cursor.fetchall()
    a = [a[1] for a in res]
    b = [b[0] for b in res]
    c = [c[2] for c in res]
    d = [d[3] for d in res]
    e = [e[4] for e in res]
    result=list(zip(a, b, c, d, e))
    
    return result


"""get the starting time when order = 1"""
def get_starting_time():
    result = []
    connection=connect()
    cursor = connection.cursor()
    command='''SELECT time_slot from timing where eorder = 1;'''
    cursor.execute(command)
    res=cursor.fetchone()[0]
    time1 = res.split("-")[0]
    hour = time1.split(":")[0]
    minute = time1.split(":")[1]
    result=[hour, minute]
    return result


"""get the defined air-con temperature according to season"""
def get_temperature():
    connection=connect()
    cursor = connection.cursor()
    command='''SELECT temperature,mode from temperature where season="{}";'''.format(util.get_season())
    cursor.execute(command)
    res=cursor.fetchone()
    return res


"""get the command that should sent to air-con"""
def get_command(temperature, mode):
    connection=connect()
    cursor = connection.cursor()
    command='''SELECT id from IRsetting where temperature="{}" and mode="{}";'''.format(temperature, mode)
    cursor.execute(command)
    res=cursor.fetchone()[0]
    return res


"""change the order of all timeslots"""
def change_order(orders):
    connection=connect()
    cursor = connection.cursor()
    hour=get_starting_time()[0]
    minute=get_starting_time()[1]
    for key in  orders.keys():
        command='''UPDATE timing SET eorder={} WHERE id={};'''.format(key+1,orders[key])
        cursor.execute(command)
        connection.commit()
    cursor.close()
    connection.commit()
    change_time(hour,minute)


"""change the time of each timeslot according to starting time"""
def change_time(hour, minute):
    try:
        hour = int(hour)
        minute = int(minute)
    except:
        return

    valid_num_hour = [a for a in range(0, 24)]
    valid_num_min = [a for a in range(0,60)]
    
    if hour not in valid_num_hour:
        return
    if minute not in valid_num_min:
        return

    connection=connect()
    cursor = connection.cursor()
    command='''SELECT length from timing where eorder=1;'''
    cursor.execute(command)
    length=cursor.fetchone()[0]

    strTime = '2020-01-01 {}:{}'.format(hour,minute)
    startTime = datetime.datetime.strptime(strTime, "%Y-%m-%d %H:%M")
    endtime = (startTime + datetime.timedelta(minutes=length)).strftime("%H:%M")

    if endtime[0]=="0":
        endtime=endtime[1:]

    s_min=str(minute)
    if len(s_min)==1:
        s_min="0"+s_min

    first_slot="{}:{}-{}".format(hour,s_min,endtime)

    command='''UPDATE timing SET time_slot="{}" WHERE eorder={};'''.format(first_slot,1)
    cursor.execute(command)
    connection.commit()

    command = '''SELECT MAX(eorder) from timing;'''
    cursor.execute(command)
    maxorder = int(cursor.fetchone()[0])

    for order in range(2,maxorder+1):
        command='''SELECT length from timing where eorder={};'''.format(order)
        cursor.execute(command)
        length=cursor.fetchone()[0]

        e=endtime

        strTime = '2020-01-01 {}'.format(endtime)
        startTime = datetime.datetime.strptime(strTime, "%Y-%m-%d %H:%M")
        endtime = (startTime + datetime.timedelta(minutes=length)).strftime("%H:%M")

        if endtime[0]=="0":
            endtime=endtime[1:]

        slot="{}-{}".format(e,endtime)

        command='''UPDATE timing SET time_slot="{}" WHERE eorder={};'''.format(slot,order)
        cursor.execute(command)
        connection.commit()

    cursor.close()
    connection.commit()
