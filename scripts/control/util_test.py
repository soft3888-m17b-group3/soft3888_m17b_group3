from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import call
from requests.auth import HTTPBasicAuth
import configparser
import unittest
import database
import util
from unittest.mock import patch
from event import Event
import json

class TestUtilMethod(unittest.TestCase):
    def setUp(self):
        
        return
    

    @patch('util.configparser')
    def test_read_config(self,mock_configparser):
        config = util.read_config()
        self.assertEqual(mock_configparser.ConfigParser.return_value ,config)
        return


    def test_turn_off(self):
        expected_calls = [call("systemControl","OFF"), call("ruleTest","ON"), call("ruleTest","OFF")]
        with patch('util.api_call') as mock_api_call:
            util.turn_off()
            mock_api_call.assert_has_calls(expected_calls)
        return
    
    @patch("util.read_config")
    @patch("util.HTTPBasicAuth")
    @patch("util.requests")
    def test_api_call(self, mock_requests, mock_auth, mock_read_config):
        headers = {'Content-Type': 'text/plain'}
        mock_read_config.return_value = {'SETTING':{'root_url':'http://localhost:8080'}}
        util.api_call("item", "command")
        mock_requests.request.assert_called_with("POST", "http://localhost:8080/rest/items/item", headers=headers, data="command", auth=mock_auth.return_value)
    
    @patch('util.requests')
    def test_get_rain(self, mock_requests):
        x = '{ "data": [ { "precip":100}]}'
        mock_requests.get.return_value.json.return_value = json.loads(x)
        mock_requests.get.return_value.status_code = 200
        self.assertEqual(util.get_rain(), 100)

    @patch('util.requests')
    def test_get_cloud(self, mock_requests):
        x = '{ "data": [ { "clouds":50}]}'
        mock_requests.get.return_value.json.return_value = json.loads(x)
        mock_requests.get.return_value.status_code = 200
        self.assertEqual(util.get_cloud(), 50)

    @patch('util.datetime')
    def test_get_season(self, mock_datetime):
        for i in range(1, 13):
            mock_datetime.datetime.now().strftime.return_value = i
            if i >= 3 and i <= 5:
                # Autumn
                self.assertEqual(util.get_season(), "autumn")
            elif i >= 6 and i <= 8:
                # Winter
                self.assertEqual(util.get_season(), "winter")
            elif i >= 9 and i <= 11:
                # Spring
                self.assertEqual(util.get_season(), "spring")
            else:
                # Summer
                self.assertEqual(util.get_season(), "summer")
        
        

if __name__ == "__main__":
    unittest.main()
    pass