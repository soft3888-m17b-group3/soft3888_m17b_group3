from unittest.mock import Mock
import configparser
import unittest
import database
from event import Event
from unittest.mock import Mock
from unittest.mock import patch
import sqlite3
import os
import shutil

class TestDataBaseMethods(unittest.TestCase):


    def setUp(self):
        return

    def tearDown(self):
        os.remove('config.db')
        os.rename('config.db.bak','config.db')
        shutil.copyfile('config.db','config.db.bak')
    
    def test_connect(self):
        self.assertTrue(isinstance(database.connect(), sqlite3.Connection))

    def test_reader(self):
        exp_timeslot = "6:00-9:00"
        event = "Wake Up Time"
        self.assertEqual(exp_timeslot, database.reader(event))

    def test_get_data(self):
        exp_data = [('Wake Up Time', '6:00-9:00', '{"lightBulbsColor":{"command":"60,0,0","fade_type":"brightness","fadein_min":"60","fadein_slot":"180","multhread":"fade","weather":"None"},"lightStripsColor":{"command":"60,0,0","fade_type":"brightness","fadein_min":"60","fadein_slot":"180","multhread":"fade_in","weather":"None"},"chromecastPlay":{"command":"https://api-soft3888.ddns.net/autumn_sunrise.mp4","fade_type":"None","multhread":"media","length":"30:16"},"nestPlay":{"command":"https://api-soft3888.ddns.net/autumn_sunrise.mp4","weather":"None","multhread":"media","length":"30:16"},"motionTrigger":{"command":"OFF","multhread":"None"}}', 1, 1), ('Prepare for Getup', '9:00-9:30', '{"lightBulbsColor":{"command":"60,100,100","fade_type":"None","multhread":"None","weather":"None"},"lightStripsColor":{"command":"60,100,100","fade_type":"None","multhread":"None","weather":"None"},"chromecastPlay":{"command":"None","fade_type":"None","multhread":"media","length":"None","weather":{"rainy":{"url":"https://api-soft3888.ddns.net/rain.mp4","length":"60:00"},"cloudy":{"url":"https://api-soft3888.ddns.net/waterfall.mp4","length":"30:12"}}},"nestPlay":{"command":"None","fade_type":"None","multhread":"media","length":"None","weather":{"rainy":{"url":"https://api-soft3888.ddns.net/rain.mp4","length":"60:00"},"cloudy":{"url":"https://api-soft3888.ddns.net/waterfall.mp4","length":"30:12"}}},"motionTrigger":{"command":"OFF","multhread":"None"}}', 2, 2), ('breakfast Time', '9:30-10:30', '{"lightBulbsColor":{"command":"None","fade_type":"None","multhread":"None","weather":{"rainy":"26,100,52","sunny":"190,100,22"}},"lightStripsColor":{"command":"None","fade_type":"None","multhread":"None","weather":{"rainy":"26,90,10","sunny":"OFF"}},"chromecastPlay":{"command":"None","fade_type":"None","multhread":"media","length":"None","weather":{"rainy":{"url":"https://api-soft3888.ddns.net/cafe_rain.mp4","length":"60:00"},"cloudy":{"url":"https://api-soft3888.ddns.net/cafe_day.mp4","length":"60:00"}}},"nestPlay":{"command":"None","fade_type":"None","multhread":"media","length":"None","weather":{"rainy":{"url":"https://api-soft3888.ddns.net/cafe_rain.mp4","length":"60:00"},"cloudy":{"url":"https://api-soft3888.ddns.net/cafe_day.mp4","length":"60:00"}}},"motionTrigger":{"command":"OFF","multhread":"None"}}', 3, 3), ('Leisure Time', '10:30-12:00', '{"lightBulbsColor":{"command":"None","fade_type":"None","multhread":"None","weather":{"rainy":"26,100,72","sunny":"190,100,10"}},"lightStripsColor":{"command":"None","fade_type":"None","multhread":"None","weather":{"rainy":"26,90,40","sunny":"OFF"}},"chromecastPlay":{"command":"None","fade_type":"None","multhread":"media","length":"None","weather":{"rainy":{"url":"https://api-soft3888.ddns.net/rain.mp4","length":"01:00"},"cloudy":{"url":"https://api-soft3888.ddns.net/sunny_930am.gif","length":"01:00"}}},"nestPlay":{"command":"None","fade_type":"None","multhread":"media","length":"None","weather":{"rainy":{"url":"https://api-soft3888.ddns.net/sunny_930am.gif","length":"30:00"},"cloudy":{"url":"https://api-soft3888.ddns.net/rain.mp4","length":"01:00"}}},"motionTrigger":{"command":"OFF","multhread":"None"}}', 4, 4), ('Lunch Time', '12:00-13:00', '{"lightBulbsColor":{"command":"None","fade_type":"None","multhread":"None","weather":{"rainy":"48,100,100","sunny":"60,0,50"}},"lightStripsColor":{"command":"None","fade_type":"None","multhread":"None","weather":{"rainy":"60,0,50","sunny":"60,0,50"}},"chromecastPlay":{"command":"None","fade_type":"None","multhread":"media","weather":{"rainy":{"url":"https://api-soft3888.ddns.net/lunch_rain.mp4","length":"01:00"},"cloudy":{"url":"https://api-soft3888.ddns.net/lunch_sunny.mp4","length":"01:00"}}},"motionTrigger":{"command":"OFF","multhread":"None"}}', 5, 5), ('Work Time', '13:00-15:00',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    '{"lightBulbsColor":{"command":"180,0,100","fade_type":"None","multhread":"None","weather":"None"},"lightStripsColor":{"command":"180,0,100","fade_type":"None","multhread":"None","weather":"None"},"chromecastPlay":{"command":"https://api-soft3888.ddns.net/black.png","fade_type":"None","multhread":"media","weather":"None","length":"30:00"},"motionTrigger":{"command":"OFF","multhread":"None"}}', 6, 6), ('Exercise Time 1', '15:00-15:30', '{"lightBulbsColor":{"command":"160,0,100","fade_type":"None","multhread":"None","weather":"None"},"lightStripsColor":{"command":"160,0,100","fade_type":"None","multhread":"None","weather":"None"},"chromecastPlay":{"command":"https://api-soft3888.ddns.net/workout.mp4","fade_type":"None","multhread":"media","length":"01:00","weather":"None"},"motionTrigger":{"command":"OFF","multhread":"None"}}', 7, 7), ('Exercise Time 2', '15:30-16:00', '{"lightBulbsColor":{"command":"160,0,100","fade_type":"None","multhread":"None","weather":"None"},"lightStripsColor":{"command":"160,0,100","fade_type":"None","multhread":"None","weather":"None"},"chromecastPlay":{"command":"https://media.self.com/photos/58e7dd9d3172253b0d06ff08/master/w_1600%2Cc_limit/Booty-Band-Side-Steps.gif","fade_type":"None","multhread":"media","length":"03:00","weather":"None"},"nestPlay":{"command":"https://api-soft3888.ddns.net/gym-sound/gym1.mp3","fade_type":"None","multhread":"media","length":"02:00","weather":"None"},"motionTrigger":{"command":"OFF","multhread":"None"}}', 8, 8), ('Sunset Time', '16:00-18:00', '{"lightBulbsColor":{"command":"160,0,100","fade_type":"color","fadein_min":"120","fadein_slot":"180","multhread":"fade","weather":"None"},"lightStripsColor":{"command":"160,0,100","fade_type":"color","fadein_min":"120","fadein_slot":"180","multhread":"None","weather":"None"},"nestPlay":{"command":"https://api-soft3888.ddns.net/sunset-sound/sunset1.mp3","fade_type":"None","multhread":"media","length":"02:00","weather":"None"},"motionTrigger":{"command":"OFF","multhread":"None"}}', 9, 9), ('Dinner Time', '18:00-19:00', '{"lightBulbsColor":{"command":"60,80,100","fade_type":"out","fadein_min":"60","fadein_slot":"108","multhread":"fade","weather":"None"},"lightStripsColor":{"command":"60,80,100","fade_type":"out","fadein_min":"60","fadein_slot":"108","multhread":"None","weather":"None"},"chromecastPlay":{"command":"None","fade_type":"None","multhread":"media","weather":{"rainy":{"url":"https://api-soft3888.ddns.net/rain.gif","length":"10:10"},"cloudy":{"url":"https://api-soft3888.ddns.net/sunny.gif","length":"10:10"}}},"nestPlay":{"command":"https://api-soft3888.ddns.net/michelin-sound/moonlight.mp3","multhread":"media","length":"10:10","weather":"None"},"motionTrigger":{"command":"OFF","multhread":"None"}}', 10, 10), ('Movie Time', '19:00-20:00', '{"lightStripsColor":{"command":"200,55,55","fade_type":"None","multhread":"None","weather":"None"},"chromecastPlay":{"command":"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4","fade_type":"None","multhread":"media","weather":"None"},"motionTrigger":{"command":"ON","multhread":"None"}}', 11, 11), ('Prepare for Sleep', '20:00-21:00', '{"lightBulbsColor":{"command":"60,0,100","fade_type":"out","fadein_min":"60","fadein_slot":"108","multhread":"fade","weather":"None"},"lightStripsColor":{"command":"60,0,100","fade_type":"out","fadein_min":"60","fadein_slot":"108","multhread":"None","weather":"None"},"chromecastPlay":{"command":"https://api-soft3888.ddns.net/campfire.mp4","fade_type":"None","multhread":"media","weather":"None"},"motionTrigger":{"command":"ON","multhread":"None"}}', 12, 12), ('Bed Time', '21:00-6:00', '{"chromecastPlay":{"command":"https://api-soft3888.ddns.net/stars.gif","fade_type":"None","multhread":"media","weather":"None"},"nestPlay":{"command":"https://api-soft3888.ddns.net/moonlight.mp3","multhread":"media","length":"10:10","weather":"None"},"motionTrigger":{"command":"ON","multhread":"None"}}', 13, 13)]
        self.assertEqual(exp_data, database.get_data())

    def test_get_wakeup_time(self):
        exp_time = ['6', '00']
        self.assertEqual(exp_time, database.get_starting_time())

    def test_get_temperature(self):
        exp_result = ('22', 'cool')
        self.assertEqual(database.get_temperature(), exp_result)

    def test_get_command(self):
        exp_result = 7
        self.assertEqual(database.get_command('22', 'cool'), exp_result)
    
    def test_change_time(self):
        database.change_time(10,10)

        conn = database.connect()
        cursor = conn.cursor()
        command = '''SELECT time_slot from timing where id={};'''.format(1)
        cursor.execute(command)
        res = cursor.fetchone()[0]
        begin_time = res.split("-")[0]
        begin_hour = begin_time.split(":")[0]
        begin_min = begin_time.split(":")[1]

        self.assertEqual(begin_hour,'10')
        self.assertEqual(begin_min, '10')
    
    def test_change_time_2(self):
        database.change_time(5, 10)

        conn = database.connect()
        cursor = conn.cursor()
        command = '''SELECT time_slot from timing where id={};'''.format(1)
        cursor.execute(command)
        res = cursor.fetchone()[0]
        begin_time = res.split("-")[0]
        begin_hour = begin_time.split(":")[0]
        begin_min = begin_time.split(":")[1]

        self.assertEqual(begin_hour, '5')
        self.assertEqual(begin_min, '10')
    
    def test_change_time_3(self):
        database.change_time(20, 50)

        conn = database.connect()
        cursor = conn.cursor()
        command = '''SELECT time_slot from timing where id={};'''.format(1)
        cursor.execute(command)
        res = cursor.fetchone()[0]
        begin_time = res.split("-")[0]
        begin_hour = begin_time.split(":")[0]
        begin_min = begin_time.split(":")[1]

        self.assertEqual(begin_hour, '20')
        self.assertEqual(begin_min, '50')

    def test_change_time_4(self):
        database.change_time(20, 61)

        conn = database.connect()
        cursor = conn.cursor()
        command = '''SELECT time_slot from timing where id={};'''.format(1)
        cursor.execute(command)
        res = cursor.fetchone()[0]
        begin_time = res.split("-")[0]
        begin_hour = begin_time.split(":")[0]
        begin_min = begin_time.split(":")[1]

        self.assertEqual(begin_hour, '6')
        self.assertEqual(begin_min, '00')
    
    def test_change_time_5(self):
        database.change_time(-20, -10)

        conn = database.connect()
        cursor = conn.cursor()
        command = '''SELECT time_slot from timing where id={};'''.format(1)
        cursor.execute(command)
        res = cursor.fetchone()[0]
        begin_time = res.split("-")[0]
        begin_hour = begin_time.split(":")[0]
        begin_min = begin_time.split(":")[1]

        self.assertEqual(begin_hour, '6')
        self.assertEqual(begin_min, '00')
    
    def test_change_time_6(self):
        database.change_time(-20, 10)

        conn = database.connect()
        cursor = conn.cursor()
        command = '''SELECT time_slot from timing where id={};'''.format(1)
        cursor.execute(command)
        res = cursor.fetchone()[0]
        begin_time = res.split("-")[0]
        begin_hour = begin_time.split(":")[0]
        begin_min = begin_time.split(":")[1]

        self.assertEqual(begin_hour, '6')
        self.assertEqual(begin_min, '00')
    
    def test_change_time_7(self):
        database.change_time(0, 0)

        conn = database.connect()
        cursor = conn.cursor()
        command = '''SELECT time_slot from timing where id={};'''.format(1)
        cursor.execute(command)
        res = cursor.fetchone()[0]
        begin_time = res.split("-")[0]
        begin_hour = begin_time.split(":")[0]
        begin_min = begin_time.split(":")[1]

        self.assertEqual(begin_hour, '0')
        self.assertEqual(begin_min, '00')
    
    def test_change_time_8(self):
        database.change_time('a', 'b')

        conn = database.connect()
        cursor = conn.cursor()
        command = '''SELECT time_slot from timing where id={};'''.format(1)
        cursor.execute(command)
        res = cursor.fetchone()[0]
        begin_time = res.split("-")[0]
        begin_hour = begin_time.split(":")[0]
        begin_min = begin_time.split(":")[1]

        self.assertEqual(begin_hour, '6')
        self.assertEqual(begin_min, '00')


    # @patch('database.change_time')
    # def test_change_time_mock(self, mock_database):
    #     mock_database.change_time(12,12)
    #     mock_con = mock_database.connect()
    #     cursor = mock_con.cursor()
    #     command = '''SELECT time_slot from timing where id={};'''.format(1)
    #     cursor.execute(command)
    #     res = cursor.fetchone()[0]
    #     begin_time = res.split("-")[0]
    #     begin_hour = begin_time.split(":")[0]
    #     begin_min = begin_time.split(":")[1]
    #     self.assertEqual(begin_hour,12)
    #     self.assertEqual(begin_min,12)


        

    # pass
if __name__ == "__main__":
    unittest.main()
