
<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <title>Admin</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-table.css" rel="stylesheet">

    <link href="/css/bootstrap-table-reorder-rows.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

</head>

<body class="main-body">
    
    <div class="page-header center">
        <h1>Starting time</h1>
    </div>
    <div class="center">
        Just set a wake up time you prefer and click "Submit", the system will do the rest for you.
        <br>
        <br>
        <form action="/admin" method="post">
            Wake up time: <input type="text" name="hour" placeholder="hh"/> : <input type="text" name="minute" placeholder="mm"/> 
            <input type="submit" value="Submit" class="btn btn-primary"/>
        </form>
    </div>

    <br>
    <br>
    <div class="page-header center">
        <h1>Events order</h1>
    </div>
    <div class="center">
       Just drag the time slot to re-order them, the system will do the rest for you.
    </div>
    <br>
    <table id="tabletest">

        <thead>

            <tr>

                <th data-field="order">Order</th>
                <th data-field="id" >id</th>
                <th data-field="name">Name and Time Slot</th>
                <th data-field="actions">Actions</th>


            </tr>

        </thead>

        <tbody>

            % for i in range(0,len(time)):
            <tr>

                <td>{{i+1}}</td>
                <td >{{ids[i]}}</td>
                <td>{{name[i]}}: 
                <br>
                {{time[i]}}</td>

                <td>
                <input type="hidden" name="id-{{i+1}}" value="{{ids[i]}}"> 
                <input type="hidden" name="order-{{i+1}}" value="{{order[i]}}">
                Actions:
                <br>
                % for j in range (0,len(action[i])):
                {{j+1}}. {{list(action[i][j].keys())[0]}}:
                {{list(action[i][j].values())[0]}}
                <br>
                % end
                </td>
            </tr>
            % end

        </tbody>

    </table>
   

   <script src="/js/jquery.min.js"></script>

    <script src="/js/bootstrap.min.js"></script>

    <script src="/js/bootstrap-table.js"></script>

    <script src="/js/bootstrap-table-en-US.js"></script>

    <script src="/js/jquery.tablednd.min.js"></script>

    <script src="/js/bootstrap-table-reorder-rows.js"></script>
        
    <script src="/js/admin.js"></script>

</body>

</html>
